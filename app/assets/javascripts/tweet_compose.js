(function(){

  $.TweetCompose = function (el) {
    this.$el = $(el);
    this.$el.on('submit', this.submit.bind(this));
    this.$textArea = this.$el.find("textarea");
    this.$tweetList = $(this.$el.data("tweets-ul"));

    this.$textArea.on("input", this.updateLetterCount.bind(this));
    this.$el.find('.add-mentioned-user').on("click", this.addMentionUser.bind(this));

    this.$el.on("click", ".remove-mentioned-user", this.removeMentionedUser.bind(this))
  };

  $.TweetCompose.prototype.updateLetterCount = function (event) {
    var charsLeft = 140 - event.currentTarget.textLength;
    this.$el.find(".chars-left").html(charsLeft);
  }

  $.TweetCompose.prototype.addMentionUser = function (event) {
    event.preventDefault();
    var script = $("script.mention-selector");
    this.$el.find('.mentioned-users').append(script.html());
  }

  $.TweetCompose.prototype.removeMentionedUser = function (event) {
    event.preventDefault();
    $(event.currentTarget).parent().remove();
  }

  $.TweetCompose.prototype.submit = function (event) {
    event.preventDefault();
    var formData = this.$el.serializeJSON();
    this.$el.find(":input").prop('disabled', true);

    $.ajax({
      method: 'POST',
      url: "/tweets",
      data: formData,
      dataType: 'JSON',
      success: this.handleSuccess.bind(this)
    });

  };

  $.TweetCompose.prototype.handleSuccess = function (response){
    this.$el.find(":input").not('.tweetSubmit').val("");
    this.$el.find(":input").prop('disabled', false);
    this.$el.find('.mentioned-users').empty();

    var html = "<li>" + response["content"] + " -- " + response["user"]["username"] + " -- " + response["created_at"] + "</li>";
    this.$tweetList.append(html);
    this.$textArea.trigger("input");
  }

  $.fn.tweetCompose = function (options) {
  return this.each(function () {
    new $.TweetCompose(this);
  });
  };

})();
