(function(){

  $.FollowToggle = function (el, options) {
    this.$el = $(el);
    this.userId = this.$el.data('userId') || options.userId;
    this.followState = this.$el.data('initialFollowState') || options.followState;
    this.render();
    this.$el.on("click", this.handleClick.bind(this));
  };

  $.FollowToggle.prototype.render = function () {
    if (this.followState === 'Followed') {
      var buttonText = "Unfollow";
    } else {
      var buttonText = "Follow";
    }

    this.$el.html(buttonText);
  };

  $.FollowToggle.prototype.handleClick = function (event) {
    var $button = $(event.currentTarget);
    event.preventDefault();

    if (this.followState === 'Followed') {
      var buttonMethod = "DELETE";
    } else {
      var buttonMethod = "POST";
    }
    this.$el.prop('disabled', true);

    $.ajax({
      method: buttonMethod,
      url: "/users/" + this.userId + "/follow",
      dataType: 'JSON',
      success: function(){
        toggleFollowState.bind(this)();
        this.render();
        this.$el.prop('disabled',false);
      }.bind(this)
    });
  };

  function toggleFollowState () {
    if (this.followState === 'Followed') {
      this.followState = 'Unfollowed';
    } else {
      this.followState = 'Followed';
    }
  };

  $.fn.followToggle = function (options) {
  return this.each(function () {
    new $.FollowToggle(this, options);
  });
  };

})();
