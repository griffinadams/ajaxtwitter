(function(){
  $.UsersSearch = function(el){
    this.$el = $(el);
    this.$input = this.$el.find("input");
    this.$list = this.$el.find(".users-list");
    this.$el.on("input", this.handleInput.bind(this));
  }

  $.UsersSearch.prototype.handleInput = function(event){
    event.preventDefault();

    var queryVal = this.$input.val();

    $.ajax({
      method: "GET",
      url: "/users/search",
      dataType: 'JSON',
      data: {'query': queryVal},
      success: function(data){
        this.renderResults(data);
      }.bind(this)
    });
  }

  $.UsersSearch.prototype.renderResults = function(results){
    this.$list.empty();

    results.forEach(function (el, idx) {

      if (el.followed){
        var fState = "Followed";
      } else {
        var fState = "Unfollowed";
      }

      var html = "<li>" + el.username + "<button id='" + idx + "' class='follow-toggle'> </button></li>";
      this.$list.append(html);
      $("button.follow-toggle#" +idx).followToggle({
        userId: el.id,
        followState: fState
      });
    }.bind(this))
  }

  $.fn.usersSearch = function () {
  return this.each(function () {
    new $.UsersSearch(this);
  });
  };

})();
