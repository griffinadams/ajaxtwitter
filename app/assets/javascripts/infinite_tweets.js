(function(){

  $.InfiniteTweets = function (el) {
    this.$el = $(el);
    this.$el.find(".fetch-more").on("click", this.fetchTweets.bind(this));
    this.$el.find(".fetch-more").trigger("click");
    this.$feed = this.$el.find("#feed");
    this.max_created_at = "null"
    this.template = _.template(this.$el.find(".tweet-template").html());
    console.log(this.template);
  };

  $.InfiniteTweets.prototype.fetchTweets = function(event) {
    event.preventDefault();

    var ajaxOpts = {
      method: "GET",
      path: "/feed",
      dataType: "JSON",
      success: this.insertTweets.bind(this)
    }

    if (this.max_created_at !== "null"){
      ajaxOpts.data = {"max_created_at" : this.max_created_at };
    }

    $.ajax(ajaxOpts);
  }

  $.InfiniteTweets.prototype.insertTweets = function(data){

    this.$feed.append(this.template({tweets: $(data)}));

    this.max_created_at = $(data).last()[0].created_at;

    if (data.length < 5) {
      alert("No more tweets to fetch");
      this.$el.find(".fetch-more").remove();
    }
  }

  $.fn.infiniteTweets = function (options) {
    return this.each(function () {
      new $.InfiniteTweets(this);
    });
  };



})();
